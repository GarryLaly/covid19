import React from "react";
import styled from "styled-components/native";

const Container = styled.View`
  flex: 1;
  background-color: white;
  padding: 40px 0;
`;

const Title = styled.Text``;

const About = () => {

  return (
    <Container>
      <Title>Tentang Kami</Title>
    </Container>
  );
};

export default About;
