import React from "react";
import styled from "styled-components/native";

const Container = styled.View`
  flex: 1;
  background-color: white;
  padding: 40px 0;
`;

const Title = styled.Text``;

const Home = ({navigation}) => {

  return (
    <Container>
      <Title>Text</Title>
      <Title onPress={() => navigation.navigate("About")}>Open About Page</Title>
      <Title onPress={() => navigation.navigate("List")}>Open List Page</Title>
    </Container>
  );
};

export default Home;
